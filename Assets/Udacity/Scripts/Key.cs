﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    //Create a reference to the KeyPoofPrefab and Door
	public GameObject keyPoofPrefab;
	public Door door = null;

	void Update()
	{
		//Bonus: Key Animation
	}

	public void OnKeyClicked()
	{
        // Instatiate the KeyPoof Prefab where this key is located
		Instantiate(keyPoofPrefab);

        // Make sure the poof animates vertically
		keyPoofPrefab.transform.Rotate (Vector3.up * 5.0f);

        // Call the Unlock() method on the Door
		door.Unlock();

        // Destroy the key. Check the Unity documentation on how to use Destroy
		Destroy(keyPoofPrefab);
    }

}
